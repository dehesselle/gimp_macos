#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2022 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Custom configuration for jhb.

### shellcheck #################################################################

# shellcheck disable=SC2034 # no unused variables

### dependencies ###############################################################

# Nothing here.

### variables ##################################################################

RECOMMENDED_SDK_VER_X86_64=10.13

### functions ##################################################################

# Nothing here.

### main #######################################################################

# Nothing here.
