#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Install GTK3.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

### variables ##################################################################

SELF_DIR=$(dirname "${BASH_SOURCE[0]}")

### functions ##################################################################

# Nothing here.

### main #######################################################################

cp "$SELF_DIR"/jhb-custom.conf.sh "$SELF_DIR"/jhb/etc

"$SELF_DIR"/jhb/usr/bin/bootstrap

jhb configure "$SELF_DIR"/modulesets/gtk-osx.modules

jhb build \
  libxml2 \
  pygments \
  python3

jhb build \
  icu \
  libnsgif \
  meta-gtk-osx-freetype \
  meta-gtk-osx-bootstrap \
  meta-gtk-osx-gtk3
