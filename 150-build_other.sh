#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Install tools that are not direct dependencies of GIMP but required for
# e.g. packaging.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

# shellcheck disable=SC1091 # dynamic include
source "$SRC_DIR"/jhb/jhbuild.sh

source "$(dirname "${BASH_SOURCE[0]}")"/src/dmgbuild.sh

### variables ##################################################################

# Nothing here.

### functions ##################################################################

# Nothing here.

### main #######################################################################

if $CI; then   # break in CI, otherwise we get interactive prompt by JHBuild
  error_trace_enable
fi

#---------------------------------------------------- install disk image creator

dmgbuild_install

#----------------------------------------------------- create application bundle

jhb build gtkmacbundler

#------------------------------------------------------------ image manipulation

jhb build imagemagick
